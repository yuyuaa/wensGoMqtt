package main

import (
	"flag"
	"github.com/wumingyu12/wensMqtt/tcpProxy"
	"log"
)

var (
	id     = flag.String("id", "", "需要被连接的clientId")
	local  = flag.Int("lp", 12345, "local port映射到的本地端口")
	remote = flag.Int("rp", 22, "remote port 映射到client对应的端口默认22")
)

func main() {
	flag.Parse()
	log.SetFlags(log.LstdFlags | log.Llongfile)
	tcpp := &tcpProxy.TcpMqttProxy{}
	err := tcpp.SetUp()
	if err != nil {
		log.Println(err)
	}
	//tcpp.Link("d0e32f3d", 7777, 22)
	tcpp.Link(*id, *local, *remote)
	select {}

}
