## 温氏mqtt客户端连接库




### 库主要功能
1.通过一个base64的注册二维码，通过这个码与集团进行交互进而可以实现设备码与具体设备的绑定
2.将本机可以调用的restful api 通过mqtt进行远程的调用。
3.将本机的ssh通过mqtt进行远程的访问

### mqtt 注册流程
1.用户进行扫码，进入一个web页面进行填信息注册

2.用户注册完成后重启一下物联网网关（或者调一下查询api接口），其会自动查询服务器获取到uid

3.用户获取到uid后程序会对uid进行解密出clientID，usename，password再用来连接

### 使用
```go
var Mymqtt *wensMqtt.Client

func SetUp() {
	cli := &wensMqtt.Client{}
	ipfunc := gofunc.IpFunc{}
	_, devId := ipfunc.GetMac()
	httpport := viper.GetString("server.port")
	log.Println(devId)
	err := cli.SetUp("./config/mqtt.json", devId, httpport)
	if err != nil {
		logger.Logger.Panic(err)
	}
	Mymqtt = cli
}
```

- 配置文件
```json
{
	"register": {
		"reg_type": "huan-jing-kong-zhi-qi"
	},
	"mqtt": {
		"addr": "wmqtt.wens.com.cn:1883",
		"uid": "d9c5bc24-2adf-11e7-9919-00155d020177"
	}
}
```


## 子包说明
- mqttClient为温氏终端特定使用的mqttclient
- tcpProxy为穿透连接mqttClient使用的包