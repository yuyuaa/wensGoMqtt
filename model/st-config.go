package model

import (
	"math/rand"
	"strconv"
	"time"
)

type Config struct {
	ClientId       string
	UserName       string
	PassWord       string
	BrokerUrl      string
	IsCleanSession bool //是否清空session
}

//懒得设置时可以使用默认的连接
func NewDefaultConfig() Config {
	var myrand = func() string {
		r := rand.New(rand.NewSource(time.Now().UnixNano()))
		return strconv.Itoa(r.Intn(100000))
	}
	conf := Config{
		PassWord:  "12345678",
		UserName:  "resetfulapi",
		ClientId:  "resetfulapi-" + myrand(),
		BrokerUrl: "tcp://wmqtt.wens.com.cn:1883",
	}
	return conf
}
