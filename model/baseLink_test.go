package model

import (
	"log"
	"testing"
)

func Test_BaseLink(t *testing.T) {
	base := new(BaseLink)
	conf := Config{
		PassWord:  "12345678",
		UserName:  "resetfulapi",
		ClientId:  "resetfulapi-test",
		BrokerUrl: "tcp://wmqtt.wens.com.cn:1883",
	}
	err := base.SetUp(conf)
	if err != nil {
		t.Error("初始化失败")
	}
	err = base.link()
	if err != nil {
		t.Error("连接失败")
	}
	//订阅
	c := make(chan bool)
	err = base.MqttSub("/test1", func(topic string, msg []byte) {
		log.Println(topic, "---", string(msg))
		c <- true
	})
	if err != nil {
		t.Error("订阅失败")
	}
	//发布
	err = base.MqttPub("/test1", []byte("hello"))
	if err != nil {
		t.Error("发布失败")
	}
	<-c
	//取消订阅
	err = base.UnSub("/test1")
	if err != nil {
		t.Error("取消订阅失败")
	}
	t.Log("测试成功")
}
