package mqttClient

type StatePublishType string

const (
	StatePublishType_EnvCtrl    StatePublishType = "envctrl"    //环境控制器State发布类型
	StatePublishType_Weight     StatePublishType = "weight"     //料塔称重数据类型
	StatePublishType_Ammeter    StatePublishType = "ammeter"    //料塔称重数据类型
	StatePublishType_Watermeter StatePublishType = "watermeter" //料塔称重数据类型
)
