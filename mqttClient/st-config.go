package mqttClient

import (
	"gitee.com/yuyuaa/easyGoTool.git/gomodel"
)

//对应配置文件mqtt.json
type Config struct {
	gomodel.JsonRelated
	Mqtt struct {
		Addr          string `json:"addr"`
		ClientId      string `json:"client_id"`
		UserName      string `json:"user_name"`
		PassWord      string `json:"pass_word"`
		APISub        string `json:"api_sub"`
		TcpBridgeCtrl string `json:"tcp_bridge_ctrl"`
		TcpSub        string `json:"tcp_sub"`
		RespTail      string `json:"resp_tail"`
		StatePub      string `json:"state_pub"`
	} `json:"mqtt"`
}
