package mqttClient

import (
	"gitee.com/yuyuaa/easyGoTool.git/gofunc"
	"gitee.com/yuyuaa/wensGoMqtt.git/model"
	"log"
)

func (self *Client) mqttinit() {
	log.Println("mqtt init")
	//配置mqtt连接
	clientId, userName, passWord := self.GetConnectInfoByUid()
	conf := model.Config{
		ClientId:       clientId,
		UserName:       userName,
		PassWord:       passWord,
		BrokerUrl:      self.Config.Mqtt.Addr,
		IsCleanSession: true, //不需要清session我们发现emqtt服务器会自动清如果隔很久还没连接上了，就算你将这个值设为false
	}
	self.Base.SetUp(conf)
	//判断能否连接到外网
	ipfunc := gofunc.IpFunc{}
	err := ipfunc.Ping("www.baidu.com")
	if err != nil {
		self.IsOnline = false
	} else {
		self.IsOnline = true
	}
	self.Base.StartLinkWithKeepalive(func() {
		log.Println("订阅tcp与http频道")
		self.setupHttpBridge()
		self.setupTcpBridge()
	})
}

//根据uid转换为clientId，用户名，用户密码
func (self *Client) parseUidToLoginInfo(clientId string, username string, password string) {
	self.Config.Mqtt.ClientId = clientId
	self.Config.Mqtt.UserName = username
	self.Config.Mqtt.PassWord = password
	self.Config.Save2Json()
	log.Println("查询clientid为", clientId)
	//MqttConnect()
}
