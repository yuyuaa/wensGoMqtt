package common

//注册用端口
const (
	Const_regist_host        = "http://wznyz.wens.com.cn"
	Const_api_phone_regist   = "/znyz/a/app/mqtt/login"
	Const_api_dev_check      = "/znyz/a/appreg/mqtt/getClientInfo"
	Const_api_send_node_info = "/znyz/a/appreg/mqtt/regNode"
)

//发送的频道
const (
	Const_host          = "tcp://wmqtt.wens.com.cn:1883"
	Const_apisub        = "/{{clientId}}/api/+"
	Const_statepub      = "/{{clientId}}/state"
	Const_resptail      = "/response"
	Const_tcpbridgectrl = "/{{clientId}}/tcpbridgectrl"     //用来监听连接请求的
	Const_tcpsub        = "/{{clientId}}/tcp/{{sessionId}}" //用来直接接受tcp信息的通道，回复的通道response
)

const (
	Const_tcpHasEstablish = "success" //成功建立连接发送的消息
)
