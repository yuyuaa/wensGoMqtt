package common

const (
	Const_TcpBridge_Establish TcpBridgeCtrlType = 1 //建立连接
	Const_TcpBridge_Close     TcpBridgeCtrlType = 2 //关闭连接
)

type TcpBridgeCtrlType int

//用来发起tcp穿透连接请求
type TcpLinkControl struct {
	Type      TcpBridgeCtrlType `json:"type"`
	SessionId string            `json:"session_id"` //会话id
	LinkPort  int               `json:"link_port"`  //需要连接到的端口号，比如ssh的22端口
}
