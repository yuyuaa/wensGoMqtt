package common

type ApiRequset struct {
	Url    string      `json:"url"`
	Method string      `json:"method"`
	Body   interface{} `json:"body"`
}
